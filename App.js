import React, {Component} from 'react';
import { WebView,NetInfo } from 'react-native';

export default class App extends Component {
  constructor (props) {
    super(props)
    this.state = {
      uri: 'https://www.ggcasa.ddns.net'
    }
  }

  componentWillMount(){

    fetch(this.state.uri)
      .then((response) => {
        if (response.status === 200) {
          this.setState({ uri: "http://192.168.0.110:8001" })
        }
      })
      .catch((error) => {
          console.log('network error: ' + error);
      })

  }

  render() {
    return (
      
        <WebView
        source = {{ uri: this.state.uri }}
        style={{marginTop: 20}}
        />
     
    );
  }
}
